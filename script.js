let firstValue = '';
let secondValue = '';
let result = 0;
let operator = '';
let calculationString = '';
let displayContent = {
	firstValueHtml: document.getElementById("first-value"),
	operatorHtml: document.getElementById("operator"),
	secondValueHtml: document.getElementById("second-value"),
	resultHtml: document.getElementById("result"),
};


/**
 * Set the value of first/second value to calcualte
 *
 * @param {String} value
 */
function setValue(value) {
	if (result) {
		clearValue();
	}

	if (!operator) {
		firstValue = firstValue.concat(value);

		createDisplayContent('firstValueHtml', firstValue);

		return;
	}

	secondValue = secondValue.concat(value);

	createDisplayContent('secondValueHtml', secondValue);	
}

/**
 * Set the operator calcualte the values
 *
 * @param {String} value
 */
 function setOperator(value) {
	operator = value;

	createDisplayContent('operatorHtml', operator);
}

/**
 * Perform the calculation
 */
function calculate() {
	if (operator === '+') {
		result = parseInt(firstValue) + parseInt(secondValue);
	}

	if (operator === '-') {
		result = parseInt(firstValue) - parseInt(secondValue);
	}

	if (operator === 'x') {
		result = parseInt(firstValue) * parseInt(secondValue);
	}

	if (operator === '/') {
		result = parseInt(firstValue) / parseInt(secondValue);
	}

	displayContent.resultHtml.innerHTML = result;
}

/**
 * Set the operator calcualte the values
 *
 * @param {String} type
 * @param {String} value
 */
function createDisplayContent(type, value) {
	displayContent[type].innerHTML = value; 
}

/**
 * Reset everything
 */
function clearValue() {
	firstValue = '';
	secondValue = '';
	operator = '';
	result = 0;
	displayContent.firstValueHtml.innerHTML = '';
	displayContent.secondValueHtml.innerHTML = '';
	displayContent.operatorHtml.innerHTML = '';
	displayContent.resultHtml.innerHTML = '';
}